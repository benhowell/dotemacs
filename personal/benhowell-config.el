;;; person config file

;; enable winner
(when (fboundp 'winner-mode)
      (winner-mode 1))


;; save desktop layout/state
(desktop-save-mode 1)


;; mic-paren
(require 'mic-paren) ; loading
(paren-activate)     ; activating


;; figwheel repl
(defun cider-figwheel-repl ()
  (interactive)
  (save-some-buffers)
  (with-current-buffer (cider-current-repl-buffer)
    (goto-char (point-max))
    (insert "(require 'figwheel-sidecar.repl-api)
             (figwheel-sidecar.repl-api/start-figwheel!) ; idempotent
             (figwheel-sidecar.repl-api/cljs-repl)")
    (cider-repl-return)))

(global-set-key (kbd "C-c C-f") #'cider-figwheel-repl)


;; preview files in dired
(use-package peep-dired
             :ensure t
             :defer t ; don't access `dired-mode-map' until `peep-dired' is loaded
             :bind (:map dired-mode-map
                         ("P" . peep-dired)))


;; clock
(setq display-time-string-forms
      '((propertize (concat " " 24-hours ":" minutes " "))))
(display-time-mode 1)


;; disable intrusive guru mode.
(defun disable-guru-mode ()
  (guru-mode -1)
  )
(add-hook 'prelude-prog-mode-hook 'disable-guru-mode t)


;;; custom jump to definition for dispatch handlers
;;; (thanks https://github.com/markhepburn)
;;; Based heavily on nrepl-make-response-handler
;; (defun mh/cider--make-jump-callback ()
;;   (lexical-let ((current-buffer (current-buffer)))
;;     (lambda (response)
;;       (with-current-buffer current-buffer
;;         (nrepl-dbind-response response  (value ns out err status id pprint-out)
;;           (cond (value (cider--find-var value))
;;                 (out (message "Out value: %s" out))
;;                 (pprint-out (message "PPrint-Out value: %s" pprint-out))
;;                 (err (message "Damn, error: %s" err))
;;                 (status
;;                  (when (member "interrupted" status)
;;                    (message "Evaluation interrupted."))
;;                  (when (member "eval-error" status)
;;                    (message "eval-error"))
;;                  (when (member "namespace-not-found" status)
;;                    (message "Namespace not found."))
;;                  (when (member "need-input" status)
;;                    (cider-need-input buffer))
;;                  (when (member "done" status)
;;                    (message "Done.")))))))))

;; (defun mh/cider--jump-to-definition-type (type)
;;   (let ((nrepl-sync-request-timeout nil)
;;         (kw (string-trim (cider-last-sexp)))
;;         (config-db "assets.core/ampere-config"))
;;     (cider-interactive-eval
;;      (format "(-> %s %s meta :api %s :name)" config-db type kw)
;;      (mh/cider--make-jump-callback))))

;; (defun mh/cider-find-handler ()
;;   (interactive)
;;   (mh/cider--jump-to-definition-type ":handlers"))

;; (defun mh/cider-find-sub ()
;;   (interactive)
;;   (mh/cider--jump-to-definition-type ":subs"))

;; ;;; hook into M-. by advising:
;; (defun mh/advise-cider-find-var-subs-handlers (orig-fun &rest args)
;;   (cond
;;    ((looking-back "dispatch[ \n]+\\[\\(:[a-zA-Z0-9/-]*\\)?")
;;     (mh/cider-find-handler))
;;    ((looking-back "\\(sample\\|observe\\)[ \n]+\\[\\(:[a-zA-Z0-9/-]*\\)?")
;;     (mh/cider-find-sub))
;;    (t (apply orig-fun args))))

;; (advice-add 'cider-find-var :around #'mh/advise-cider-find-var-subs-handlers)
